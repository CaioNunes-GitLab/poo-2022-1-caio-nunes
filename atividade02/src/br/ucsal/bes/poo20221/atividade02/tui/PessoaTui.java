package br.ucsal.bes.poo20221.atividade02.tui;

import java.util.Scanner;

import br.ucsal.bes.poo20221.atividade02.domain.Endereco;
import br.ucsal.bes.poo20221.atividade02.domain.Pessoa;
import br.ucsal.bes.poo20221.atividade02.domain.Veiculo;

public class PessoaTui {
	public static Pessoa cadastrarPessoa() {
		Scanner sc = new Scanner(System.in);

		System.out.println("Qual o nome do propriet�rio do ve�culo?");
		String nome = sc.nextLine();
		System.out.println("Qual o CPF do propriet�rio do ve�culo?");
		String cpf = sc.nextLine();
		Endereco endereco = EnderecoTui.adicionarEndereco();
		Pessoa pessoa = new Pessoa(cpf,nome,endereco);

		System.out.println("Quantos telefone(s) deseja cadastrar?");
		int quantTelefone = sc.nextInt();
		sc.nextLine();
		
		for(int i = 0; quantTelefone > i; i++) {
			System.out.println("Digite o " + (i+1) +"� telefone que deseja cadastrar:");
			String telefone = sc.nextLine();
			pessoa.addTelefones(telefone);
		}
		return pessoa;
	}

	public static void montarOutput(Pessoa pessoa) {
		System.out.println("Dados do propriet�rio: \n" + "Nome: "+pessoa.getNome() + "\n" 
				+ "CPF: "+ pessoa.getCpf() + "\n"
				+ "Endere�o: "+pessoa.getEndereco().getLogradouro() + ", " 
				+ pessoa.getEndereco().getBairro() + ", "
				+ pessoa.getEndereco().getComplemento() + ", " + pessoa.getEndereco().getNumero() + ", CEP: "
				+ pessoa.getEndereco().getCep() + "\n" + "\nSeu(s) n�mero(s) de telefone: ");
		for (String p : pessoa.getTelefones()) {
			System.out.println(p);
		}
		System.out.println("\nOs dados do(s) ve�culo(s): ");
		for (Veiculo v : pessoa.getVeiculos()) {
			System.out.println(
					"Placa: " + v.getPlaca() + "\n" +"Ano de fabrica��o: "+ v.getAnoFabricacao() +"\n" 
					+"Valor: "+ v.getValor() + "\n");
		}
	}
}
