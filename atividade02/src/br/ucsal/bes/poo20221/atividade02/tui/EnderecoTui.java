package br.ucsal.bes.poo20221.atividade02.tui;

import java.util.Scanner;

import br.ucsal.bes.poo20221.atividade02.domain.Endereco;

public class EnderecoTui {
	
	public static Endereco adicionarEndereco() {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Digite seu CEP");
		String cepEnd = sc.nextLine();
		System.out.println("Digite seu Logradouro");
		String logradouroEnd = sc.nextLine();
		System.out.println("Digite seu n�mero");
		String numeroEnd = sc.nextLine();
		System.out.println("Digite o complemento");
		String complementoEnd = sc.nextLine();
		System.out.println("Digite seu bairro");
		String bairroEnd = sc.nextLine();
		
	
		Endereco endereco = new Endereco(cepEnd, logradouroEnd, numeroEnd, complementoEnd, bairroEnd);		
		
		return endereco;
	}
}
