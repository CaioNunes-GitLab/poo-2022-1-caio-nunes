package br.ucsal.bes.poo20221.atividade02.tui;

import java.util.Scanner;

import br.ucsal.bes.poo20221.atividade02.domain.Pessoa;
import br.ucsal.bes.poo20221.atividade02.domain.Veiculo;

public class VeiculoTui {

	public static void cadastrarVeiculo(Pessoa pessoa) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Quantos ve�culos deseja cadastrar?");
		int numeroVeiculos = sc.nextInt();
		sc.nextLine();
		
		for(int i= 0; numeroVeiculos > i; i++) {	
			System.out.print("Informe a placa do ve�culo " + (i+1) + ": ");
			String placa = sc.nextLine();
			System.out.print("Informe o ano de fabrica��o: ");
			Integer anoFabricacao = sc.nextInt();
			sc.nextLine();
			System.out.print("Informe o valor: ");
			Double valorVeiculo = sc.nextDouble();
			sc.nextLine();
			
			
			Veiculo veiculo = new Veiculo(placa, anoFabricacao, valorVeiculo, pessoa);
			pessoa.addVeiculos(veiculo);
			
			System.out.println();
		}
				
	}
}
