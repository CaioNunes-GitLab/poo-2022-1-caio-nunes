package br.ucsal.bes.poo20221.atividade02.domain;

public class Veiculo {
	
	String placa;
	
	Integer anoFabricacao;
	
	Double valor;
	
	Pessoa proprietario;

	public Veiculo() {
	}
	
	public Veiculo(String placa, Integer anoFabricacao, Double valor, Pessoa proprietario) {
		this.placa = placa;
		this.anoFabricacao = anoFabricacao;
		this.valor = valor;
		this.proprietario = proprietario;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Pessoa getProprietario() {
		return proprietario;
	}

	public void setProprietario(Pessoa proprietario) {
		this.proprietario = proprietario;
	}	
}
