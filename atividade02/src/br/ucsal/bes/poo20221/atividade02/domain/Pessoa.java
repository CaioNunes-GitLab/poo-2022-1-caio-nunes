package br.ucsal.bes.poo20221.atividade02.domain;

import java.util.ArrayList;
import java.util.List;

public class Pessoa {

	private String cpf;

	private String nome;

	private Endereco endereco;

	private List<String> telefones = new ArrayList<>();
	private List<Veiculo> veiculos = new ArrayList<>();
	
	public Pessoa() {
	}

	public Pessoa(String cpf, String nome, Endereco endereco) {
		this.cpf = cpf;
		this.nome = nome;
		this.endereco = endereco;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void addTelefones(String telefone) {
		telefones.add(telefone);
	}
	
	public List<Veiculo> getVeiculos() {
		return veiculos;
	}

	public void addVeiculos(Veiculo veiculo) {
		veiculos.add(veiculo);
	}

	public String ToString() {
		return getCpf() + getNome() + getEndereco().getLogradouro();
	}
}
