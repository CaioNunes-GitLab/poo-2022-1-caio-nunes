package br.ucsal.bes.poo20221.atividade02.application;

import br.ucsal.bes.poo20221.atividade02.domain.Pessoa;
import br.ucsal.bes.poo20221.atividade02.tui.PessoaTui;
import br.ucsal.bes.poo20221.atividade02.tui.VeiculoTui;

public class Program {
	static Pessoa pessoa;
	
	public static void main(String[] args) {
		pessoa = PessoaTui.cadastrarPessoa();
		VeiculoTui.cadastrarVeiculo(pessoa);
        PessoaTui.montarOutput(pessoa);

	}

}
